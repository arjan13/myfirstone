import styled from 'styled-components';
import React from "react";
import Link from 'next/link';

const Nav = styled.nav `
    margin: 5px 5px;
    padding: 2px 5px;
    border-radius: 10px;
    height: 50px;
    background:#000;
    color: #fff;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;
const StyledLink = styled.a`
    padding: 0rem 2rem;
`;

const Navbar = () => {
    return (
        <Nav>
            <div>
                <Link href='/' passHref>
                <StyledLink>NXT</StyledLink>
                </Link>
            </div>
            <div>
                <Link href='/about' passHref>
                <StyledLink>About</StyledLink>
                </Link>
                <Link href='/contact' passHref>
                <StyledLink>Contact</StyledLink>
                </Link>
            </div>
        </Nav>
    );
}

export default Navbar
