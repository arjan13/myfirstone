import React from 'react'
import styled from 'styled-components';



const Welkomtext = styled.div`
   
padding: 10px 15px;
    margin:10px 5px 10px 15px;
    border-radius:10px;
    border: 0.5px solid black;
    width: 36vw; 
`;


const Artical = () => {
    return (
        <Welkomtext>
            <h1>Welkom bij mijn eerste pagina.</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum blandit blandit quam ac eleifend. Vestibulum est urna, tempor nec magna eu, porttitor commodo est. Nullam dapibus, arcu vel pretium elementum, purus ex varius libero, ut accumsan lectus ipsum sit amet nulla. Nulla mattis erat non quam venenatis finibus. Ut sit amet felis sodales, mattis lacus sagittis, pulvinar risus. Mauris commodo, mi eget volutpat auctor, lectus ipsum ullamcorper nisl, a elementum massa dolor vel nulla. Integer ac euismod nisi. Nam tincidunt.</p>
        </Welkomtext>
    )
}

export default Artical
