import React from "react";
import styled from 'styled-components';

const FooterSection = styled.div`
    background: #000;
    color: #fff;
    margin: 5px 5px;
    padding: 2px 5px;
    width: 98vw;
    border-radius: 10px;
    height: 40px;
    display: flex;
    position: fixed;
    bottom:0;
    justify-content: center;
    align-items: center;
`;

const Footer = () => {
    return (
        <FooterSection>
            <p>2021 © by A.H Looren de Jong</p>
        </FooterSection>
    );
}

export default Footer
